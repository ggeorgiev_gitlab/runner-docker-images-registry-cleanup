FROM golang:1.19 as builder

WORKDIR /app
ADD . .

RUN go test ./... -v

RUN go build -o ic

FROM ubuntu:22.10

RUN apt-get update -y && \
    apt-get install wget zip -y

# Install aws-cli
RUN wget -q https://awscli.amazonaws.com/awscli-exe-linux-x86_64-2.9.4.zip -O awscliv2.zip && \
    unzip awscliv2.zip && \
    ./aws/install && \
    rm -r aws && \
    rm awscliv2.zip && \
    aws configure set default.region us-east-1

COPY --from=builder /app/ic /usr/local/bin/ic

RUN ic -help && aws --version