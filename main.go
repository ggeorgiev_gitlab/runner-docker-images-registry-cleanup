package main

import (
	"errors"
	"flag"
	"fmt"
	"github.com/rs/zerolog/log"
	"images-cleanup/app"
	"os"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Error().Err(err).Msg("image cleanup exited with error")
		os.Exit(1)
	}
}

func run() error {
	// accessToken is not required for public projects
	var accessToken string
	flag.StringVar(&accessToken, "access-token", "", "gitlab access token")

	var repository string
	flag.StringVar(&repository, "repository", "", "repository name to search in or delete from. Values: gitlab-runner, gitlab-runner-helper")

	var registry string
	flag.StringVar(&registry, "registry", "", "registry to search in or delete from. Values: ecr, gitlab")

	var find bool
	flag.BoolVar(&find, "find", false, "find images to cleanup")

	var delete bool
	flag.BoolVar(&delete, "delete", false, "delete images passed through stdin line by line")

	var dryRun bool
	flag.BoolVar(&dryRun, "dry-run", false, "do a dry run of images removal")

	flag.Parse()

	if !strings.EqualFold(repository, string(app.ECRRepositoryRunner)) && !strings.EqualFold(repository, string(app.ECRRepositoryRunnerHelper)) {
		return fmt.Errorf("invalid `-repository` value %s", repository)
	}

	if find {
		if strings.EqualFold(registry, app.RegistryECR) {
			ecr := app.NewECR(app.ECRRepository(repository), false)
			api := app.NewAPI().WithToken(accessToken)

			return app.FindECRImagesGroups(api, ecr)
		}

		return errors.New("only ECR is supported as a registry")
	} else if delete {
		if strings.EqualFold(registry, app.RegistryECR) {
			ecr := app.NewECR(app.ECRRepository(repository), dryRun)

			return app.DeleteECRImages(ecr, os.Stdin)
		}

		return errors.New("only ECR is supported as a registry")
	}

	return nil
}
