package app

import (
	"bufio"
	"encoding/json"
	"fmt"
	"github.com/rs/zerolog/log"
	"os"
	"path/filepath"
)

type cacheName string

const cacheExtension = "jsonl"

var cacheNameTags = cacheName(fmt.Sprintf("tags.%s", cacheExtension))

func cacheNameECR(repo string) cacheName {
	return cacheName(fmt.Sprintf("ecr-images-%s.%s", repo, cacheExtension))
}

func cacheNameReleaseImages(repo string) cacheName {
	return cacheName(fmt.Sprintf("release-images-%s.%s", repo, cacheExtension))
}

func cacheNameReleaseCommitImages(repo string) cacheName {
	return cacheName(fmt.Sprintf("release-commit-images-%s.%s", repo, cacheExtension))
}

func cacheNameNotOldImages(repo string) cacheName {
	return cacheName(fmt.Sprintf("not-old-images-%s.%s", repo, cacheExtension))
}

func cacheNameToDeleteImages(repo string) cacheName {
	return cacheName(fmt.Sprintf("to-delete-images-%s.%s", repo, cacheExtension))
}

var cacheRootPath = "cache"

func cachePath(c cacheName) string {
	dir, _ := os.Getwd()
	return filepath.Join(dir, cacheRootPath, string(c))
}

func load[T any](c cacheName, fetch func() ([]T, error)) ([]T, error) {
	t, err := tryLoadCache[T](c)
	if err != nil {
		return nil, err
	}

	if len(t) > 0 {
		return t, nil
	}

	t, err = fetch()
	if err != nil {
		return nil, err
	}

	if err := writeCache(c, t); err != nil {
		return nil, err
	}

	return t, nil
}

func writeCache[T any](c cacheName, data []T) error {
	log.Info().Msgf("Writing cache %s", string(c))

	p := cachePath(c)
	cacheDir := filepath.Dir(p)
	if _, err := os.Stat(cacheDir); os.IsNotExist(err) {
		err = os.MkdirAll(cacheDir, 0o744)
		if err != nil {
			return err
		}
	}

	f, err := os.OpenFile(p, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0o644)
	if err != nil {
		return err
	}
	defer f.Close()

	w := bufio.NewWriterSize(f, 1024*1024)

	for _, d := range data {
		b, err := json.Marshal(d)
		if err != nil {
			return err
		}

		_, err = w.Write(append(b, []byte("\n")...))
		if err != nil {
			return err
		}
	}

	return w.Flush()
}

func tryLoadCache[T any](c cacheName) ([]T, error) {
	p := cachePath(c)
	_, err := os.Stat(p)
	if os.IsNotExist(err) {
		return nil, nil
	}

	log.Info().Msgf("Loading cache %s", string(c))

	f, err := os.Open(p)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	var results []T
	s := bufio.NewScanner(f)
	for s.Scan() {
		var d T
		err = json.Unmarshal(s.Bytes(), &d)
		if err != nil {
			return nil, err
		}

		results = append(results, d)
	}

	return results, s.Err()
}
