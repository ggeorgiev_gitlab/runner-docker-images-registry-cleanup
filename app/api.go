package app

import (
	"fmt"
	"github.com/go-resty/resty/v2"
	"github.com/samber/lo"
	"github.com/tidwall/gjson"
	"net/url"
)

const runnerCanonicalRepoID = "250833"

type API struct {
	gitlabAddr *url.URL
	client     *resty.Client

	url *url.URL
}

func NewAPI() *API {
	gitlabAddr, _ := url.Parse("https://gitlab.com")

	api := &API{
		client:     resty.New(),
		gitlabAddr: gitlabAddr,
	}

	return api
}

func (a *API) WithToken(token string) *API {
	if token != "" {
		a.client.SetHeader("PRIVATE-TOKEN", token)
	}

	return a
}

// withURL makes a shallow copy of API
func (a *API) withURL(url *url.URL) *API {
	return &API{
		gitlabAddr: a.gitlabAddr,
		client:     a.client,
		url:        url,
	}
}

type tag struct {
	Name   string `json:"name"`
	Commit string `json:"commit"`
}

func (a *API) fetchTags() ([]tag, error) {
	var parse = func(b []byte) []tag {
		return lo.Map(gjson.ParseBytes(b).Array(), func(value gjson.Result, _ int) tag {
			return tag{
				Name:   value.Get("name").String(),
				Commit: value.Get("commit.short_id").String(),
			}
		})
	}

	return requestMany(a.withURL(a.tagsURL()), parse)
}

func requestMany[R any](a *API, parse func(b []byte) []R) ([]R, error) {
	var responses []R

	var page int
	for {
		page++

		response, err := a.client.R().
			SetQueryParam("per_page", "100").
			SetQueryParam("page", fmt.Sprint(page)).
			SetResult([]map[string]any{}).
			Get(a.url.String())

		if err != nil {
			return nil, err
		}

		responses = append(responses, parse(response.Body())...)

		h := response.Header()
		if fmt.Sprint(page) == h.Get("x-total-pages") {
			break
		}
	}

	return responses, nil
}

func (a *API) projectURL() *url.URL {
	return a.gitlabAddr.JoinPath("/api/v4/projects/", runnerCanonicalRepoID)
}

func (a *API) tagsURL() *url.URL {
	return a.projectURL().JoinPath("repository/tags")
}
