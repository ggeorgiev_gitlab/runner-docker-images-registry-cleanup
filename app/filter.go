package app

import (
	"github.com/samber/lo"
	"strings"
	"time"
)

type filters []imageFilterFunc

func newFilters(tags []tag, maxImageAge time.Time) filters {
	return filters{
		isImageReleaseFilterFunc(tags),
		isImageCommitReleaseFilterFunc(tags),
		isImageOldFilterFunc(maxImageAge),
	}
}

type imageFilterFunc = func(img image) imageType

func (f filters) match(img image) imageType {
	for _, filter := range f {
		t := filter(img)
		if t == imageTypeNoMatch {
			continue
		}

		return t
	}

	return imageTypeNoMatch
}

func (f filters) group(images []image) map[imageType][]image {
	result := map[imageType][]image{}

	for _, img := range images {
		t := f.match(img)
		result[t] = append(result[t], img)
	}

	return result
}

func iterImageTagsWithFragments(i image, pred func(string) bool) bool {
	for _, t := range i.Tags {
		fragments := strings.Split(t, "-")
		for _, f := range fragments {
			if pred(f) {
				return true
			}
		}
	}

	return false
}

func isImageReleaseFilterFunc(tags []tag) imageFilterFunc {
	tagsMap := lo.SliceToMap(tags, func(tag tag) (string, bool) {
		return tag.Name, true
	})

	return func(img image) imageType {
		// ex: alpine3.12-x86_64-v15.2.0
		isRelease := iterImageTagsWithFragments(img, func(s string) bool {
			return tagsMap[s]
		})

		if isRelease {
			return imageTypeRelease
		}

		return imageTypeNoMatch
	}
}

func isImageOldFilterFunc(maxAge time.Time) imageFilterFunc {
	return func(img image) imageType {
		if !img.PushedAt.Before(maxAge) {
			return imageTypeNotOld
		}

		return imageTypeNoMatch
	}
}

func isImageCommitReleaseFilterFunc(tags []tag) imageFilterFunc {
	tagsMap := lo.SliceToMap(tags, func(tag tag) (string, bool) {
		return tag.Commit, true
	})

	return func(img image) imageType {
		isReleaseCommit := iterImageTagsWithFragments(img, func(s string) bool {
			return tagsMap[s]
		})

		if isReleaseCommit {
			return imageTypeReleaseCommit
		}

		return imageTypeNoMatch
	}
}
