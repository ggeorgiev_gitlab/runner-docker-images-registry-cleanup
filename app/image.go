package app

type imageType int

const (
	imageTypeRelease imageType = iota
	imageTypeReleaseCommit
	imageTypeNotOld
	imageTypeNoMatch
)

var imageTypeToCacheName = map[imageType]func(string) cacheName{
	imageTypeRelease:       cacheNameReleaseImages,
	imageTypeReleaseCommit: cacheNameReleaseCommitImages,
	imageTypeNotOld:        cacheNameNotOldImages,
	imageTypeNoMatch:       cacheNameToDeleteImages,
}

type image struct {
	PushedAt TimeRFC3339 `json:"pushed_at"`
	Tags     []string    `json:"tags"`
}
