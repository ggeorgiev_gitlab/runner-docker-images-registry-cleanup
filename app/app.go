package app

import (
	"bufio"
	"encoding/json"
	"fmt"
	"github.com/rs/zerolog/log"
	"io"
	"time"
)

var maxImageAge = time.Now().Add(-(((24 * time.Hour) * 30) * 6))

const (
	RegistryECR    = "ecr"
	RegistryGitLab = "gitlab"
)

func FindECRImagesGroups(api *API, ecr *ECR) error {
	tags, err := load(cacheNameTags, api.fetchTags)
	if err != nil {
		return err
	}

	ecrImages, err := load(cacheNameECR(string(ecr.repository)), ecr.fetchImages)
	if err != nil {
		return err
	}

	log.Info().Msgf("Tags: %d", len(tags))
	log.Info().Msgf("Images: %d", len(ecrImages))

	filters := newFilters(tags, maxImageAge)
	grouped := filters.group(ecrImages)
	for t, images := range grouped {
		cacheName := imageTypeToCacheName[t](string(ecr.repository))
		if err := writeCache(cacheName, images); err != nil {
			return fmt.Errorf("writing to cache %s: %w", cacheName, err)
		}

		log.Info().Msgf("%s: %d", cacheName, len(images))
	}

	return nil
}

func DeleteECRImages(ecr *ECR, input io.Reader) error {
	s := bufio.NewScanner(input)
	for s.Scan() {
		var img image
		if err := json.Unmarshal(s.Bytes(), &img); err != nil {
			return err
		}

		if err := ecr.deleteImage(img); err != nil {
			return err
		}
	}

	return nil
}
