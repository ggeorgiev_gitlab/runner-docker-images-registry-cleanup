package app

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"os"
	"reflect"
	"testing"
)

func TestNewECR(t *testing.T) {
	ecr := NewECR(ECRRepositoryRunner, false)

	assert.Equal(t, ecr.repository, ECRRepositoryRunner)
	assert.Equal(t, reflect.ValueOf(ecr.describeECRImages).Pointer(), reflect.ValueOf(describeECRImages).Pointer())
	assert.Equal(t, reflect.ValueOf(ecr.deleteECRTag).Pointer(), reflect.ValueOf(deleteECRTag).Pointer())

	ecr = NewECR(ECRRepositoryRunnerHelper, true)
	assert.Equal(t, ecr.repository, ECRRepositoryRunnerHelper)
	assert.Equal(t, reflect.ValueOf(ecr.describeECRImages).Pointer(), reflect.ValueOf(describeECRImages).Pointer())
	assert.Equal(t, reflect.ValueOf(ecr.deleteECRTag).Pointer(), reflect.ValueOf(deleteECRTagDryRun).Pointer())
}

func mockDescribeECRImages(_ ECRRepository) ([]byte, error) {
	return os.ReadFile("test/ecr_images_test.json")
}

func mockDescribeECRImagesInvalid(_ ECRRepository) ([]byte, error) {
	return os.ReadFile("test/ecr_images_invalid_test.json")
}

func TestECRFetchImages(t *testing.T) {
	ecr := NewECR(ECRRepositoryRunnerHelper, false)
	ecr.describeECRImages = mockDescribeECRImages

	images, err := ecr.fetchImages()
	require.NoError(t, err)

	assert.Len(t, images, 2)

	var pushedAt TimeRFC3339
	_ = pushedAt.UnmarshalJSON([]byte("2021-01-20T16:30:29+02:00"))
	assert.Equal(t, images[0].PushedAt, pushedAt)
	require.Len(t, images[0].Tags, 1)
	assert.Equal(t, images[0].Tags[0], "x86_64-775dd39d-servercore1809")

	_ = pushedAt.UnmarshalJSON([]byte("2021-07-14T17:52:13+03:00"))
	assert.Equal(t, images[1].PushedAt, pushedAt)
	require.Len(t, images[1].Tags, 2)
	assert.Equal(t, images[1].Tags[0], "ubuntu-x86_64-c1edb478-pwsh")
	assert.Equal(t, images[1].Tags[1], "ubuntu-x86_64-v14.0.1-pwsh")
}

func TestECRFetchImagesInvalidImage(t *testing.T) {
	ecr := NewECR(ECRRepositoryRunnerHelper, false)
	ecr.describeECRImages = mockDescribeECRImagesInvalid

	images, err := ecr.fetchImages()
	require.ErrorContains(t, err, "parsing `imagePushedAt=invalid`")
	require.ErrorContains(t, err, "x86_64-775dd39d-servercore1809")

	assert.Len(t, images, 1)

	var pushedAt TimeRFC3339
	_ = pushedAt.UnmarshalJSON([]byte("2021-07-14T17:52:13+03:00"))
	assert.Equal(t, images[0].PushedAt, pushedAt)
	require.Len(t, images[0].Tags, 2)
	assert.Equal(t, images[0].Tags[0], "ubuntu-x86_64-c1edb478-pwsh")
	assert.Equal(t, images[0].Tags[1], "ubuntu-x86_64-v14.0.1-pwsh")
}

func TestECRDeleteImage(t *testing.T) {
	ecr := NewECR(ECRRepositoryRunnerHelper, false)
	ecr.describeECRImages = mockDescribeECRImages

	var calls int
	ecr.deleteECRTag = func(repository ECRRepository, s string) error {
		calls++
		return nil
	}

	images, err := ecr.fetchImages()
	require.NoError(t, err)

	for _, img := range images {
		require.NoError(t, ecr.deleteImage(img))
	}

	assert.Equal(t, 3, calls)
}
