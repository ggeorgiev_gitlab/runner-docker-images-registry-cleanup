package app

import (
	"bufio"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"os"
	"path/filepath"
	"testing"
	"time"
)

func randomCache(t *testing.T) (cacheName, func()) {
	rnd := cacheName(fmt.Sprintf("%s.jsonl", hex.EncodeToString([]byte(os.TempDir()))))
	return rnd, func() {
		assert.NoError(t, os.RemoveAll(filepath.Dir(cachePath(rnd))))
	}
}

func generateCacheTestData() []testCacheData {
	return []testCacheData{
		{
			Str: "1",
		},
		{
			Str: "2",
		},
		{
			Str: "3",
		},
	}
}

type testCacheData struct {
	Str string `json:"str"`
}

func writeCacheTest(t *testing.T) (cacheName, []testCacheData, func()) {
	cache, cleanup := randomCache(t)
	data := generateCacheTestData()

	err := writeCache(cache, data)
	require.NoError(t, err)

	return cache, data, cleanup
}

func TestWriteCache(t *testing.T) {
	cache, data, cleanup := writeCacheTest(t)
	defer cleanup()

	f, err := os.Open(cachePath(cache))
	require.NoError(t, err)

	s := bufio.NewScanner(f)
	var dataRead []testCacheData
	for s.Scan() {
		var d testCacheData
		require.NoError(t, json.Unmarshal(s.Bytes(), &d))
		dataRead = append(dataRead, d)
	}

	assert.Equal(t, data, dataRead)
}

func TestWriteReadCache(t *testing.T) {
	cache, data, cleanup := writeCacheTest(t)
	defer cleanup()

	loadedData, err := tryLoadCache[testCacheData](cache)
	require.NoError(t, err)

	assert.Equal(t, data, loadedData)
}

func TestLoadCacheFetch(t *testing.T) {
	cache, cleanup := randomCache(t)
	defer cleanup()

	data, err := load(cache, func() ([]testCacheData, error) {
		return generateCacheTestData(), nil
	})
	require.NoError(t, err)
	assert.Equal(t, generateCacheTestData(), data)

	fromFileData, err := tryLoadCache[testCacheData](cache)
	require.NoError(t, err)
	assert.Equal(t, data, fromFileData)

	time.Sleep(500 * time.Millisecond)

	// second time to confirm it's loaded from cache
	data, err = load(cache, func() ([]testCacheData, error) {
		return append(generateCacheTestData(), testCacheData{Str: "4"}), nil
	})
	require.NoError(t, err)

	assert.Equal(t, data, fromFileData)
}
