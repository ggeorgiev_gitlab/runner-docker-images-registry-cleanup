package app

import (
	"bytes"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"os"
	"path/filepath"
	"testing"
)

func TestFindECRImagesGroups(t *testing.T) {
	api, cleanup := mockAPI(t)
	defer cleanup()

	ecr := NewECR(ECRRepositoryRunner, false)
	ecr.describeECRImages = mockDescribeECRImages

	cacheRootPath = "TestFindECRImagesGroups"
	pwd, _ := os.Getwd()
	fullCachePath := filepath.Join(pwd, cacheRootPath)
	require.NoError(t, os.MkdirAll(fullCachePath, os.ModePerm))
	defer func() {
		require.NoError(t, os.RemoveAll(fullCachePath))
		cacheRootPath = "cache"
	}()

	require.NoError(t, FindECRImagesGroups(api, ecr))

	tags, err := load[tag](cacheNameTags, nil)
	require.NoError(t, err)
	assert.Len(t, tags, 1)

	images, err := load[image](cacheNameToDeleteImages(string(ECRRepositoryRunner)), nil)
	require.NoError(t, err)

	assert.Len(t, images, 2)

	require.Len(t, images[0].Tags, 1)
	assert.Equal(t, images[0].Tags[0], "x86_64-775dd39d-servercore1809")

	require.Len(t, images[1].Tags, 2)
	assert.Equal(t, images[1].Tags[0], "ubuntu-x86_64-c1edb478-pwsh")
	assert.Equal(t, images[1].Tags[1], "ubuntu-x86_64-v14.0.1-pwsh")
}

func TestDeleteECRImages(t *testing.T) {
	ecr := NewECR(ECRRepositoryRunnerHelper, false)
	ecr.describeECRImages = mockDescribeECRImages

	var calls int
	ecr.deleteECRTag = func(repository ECRRepository, s string) error {
		calls++
		return nil
	}

	images, err := ecr.fetchImages()
	require.NoError(t, err)

	var buf bytes.Buffer
	for _, img := range images {
		b, err := json.Marshal(img)
		require.NoError(t, err)
		buf.Write(append(b, []byte("\n")...))
	}

	require.NoError(t, DeleteECRImages(ecr, &buf))
	assert.Equal(t, 3, calls)
}
