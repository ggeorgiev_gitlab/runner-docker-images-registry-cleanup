package app

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"github.com/samber/lo"
	"github.com/tidwall/gjson"
	"os/exec"
	"strings"
)

type ECRRepository string

const (
	ECRRepositoryRunner       = ECRRepository("gitlab-runner")
	ECRRepositoryRunnerHelper = ECRRepository("gitlab-runner-helper")
)

func describeECRImages(repository ECRRepository) ([]byte, error) {
	cmd := exec.Command("aws", "ecr-public", "describe-images", "--repository", string(repository))
	out, err := cmd.CombinedOutput()
	if err != nil {
		return nil, fmt.Errorf("running `aws describe-images` %s: %w", string(out), err)
	}

	return out, nil
}

func deleteECRTag(repository ECRRepository, tag string) error {
	cmd := exec.Command(
		"aws",
		"ecr-public",
		"batch-delete-image",
		"--repository-name", string(repository),
		"--image-ids", fmt.Sprintf("imageTag=%s", tag),
	)
	out, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("running `aws batch-delete-image` %w: %s", err, string(out))
	}

	return nil
}

func deleteECRTagDryRun(_ ECRRepository, _ string) error {
	return nil
}

type ECR struct {
	repository        ECRRepository
	describeECRImages func(ECRRepository) ([]byte, error)
	deleteECRTag      func(ECRRepository, string) error
}

func NewECR(repository ECRRepository, dryRun bool) *ECR {
	ecr := &ECR{
		repository:        repository,
		describeECRImages: describeECRImages,
		deleteECRTag:      deleteECRTag,
	}

	if dryRun {
		ecr.deleteECRTag = deleteECRTagDryRun
	}

	return ecr
}

func (ecr *ECR) fetchImages() ([]image, error) {
	out, err := ecr.describeECRImages(ecr.repository)
	if err != nil {
		return nil, err
	}

	var images []image
	var errors []error
	for _, value := range gjson.ParseBytes(out).Get("imageDetails").Array() {
		tagsJSON := value.Get("imageTags").Array()
		// TODO: there are images with no tags, we should delete them most likely
		if len(tagsJSON) == 0 {
			continue
		}

		tags := lo.Map(tagsJSON, func(value gjson.Result, _ int) string {
			return value.String()
		})

		pushedAtJSON := value.Get("imagePushedAt").String()
		pushedAt := TimeRFC3339{}
		if err := pushedAt.UnmarshalJSON([]byte(pushedAtJSON)); err != nil {
			errors = append(errors, fmt.Errorf("parsing `imagePushedAt=%s` for tags %+v", pushedAtJSON, tags))
			continue
		}

		images = append(images, image{
			PushedAt: pushedAt,
			Tags:     tags,
		})
	}

	if len(errors) == 0 {
		return images, nil
	}

	return images, fmt.Errorf("error parsing one or more images: %s", strings.Join(lo.Map(errors, func(err error, _ int) string {
		return err.Error()
	}), ", "))
}

func (ecr *ECR) deleteImage(img image) error {
	// for the life of me I couldn't get the AWS CLI delete more than a single image per command
	// even using the skeleton the CLI generates produces errors
	// however, even deleting a single image per command is fine as it won't reach rate limits this way
	// also as long as we delete images regularly we won't have to delete more than a dozen
	// I initially deleted 7000 images within a couple of hours
	// 10 images will likely not take more than a minute

	for _, tag := range img.Tags {
		log.Info().Msgf("Deleting tag %s...", tag)
		err := ecr.deleteECRTag(ecr.repository, tag)
		if err != nil {
			return fmt.Errorf("tag: %s: %w", tag, err)
		}
	}

	return nil
}
